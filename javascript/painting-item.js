const htmlStructure = `
    <div class="painting-item">
    <!-- title -->
    <div class="painting-title">
    <a href="#">옥션중</a>
    <p class="text-xs truncate">0xa998dc970xa998dc97</p>
    <img src="../assets/icon/clock.png" alt="painting" />
    <p class="days text-xs">3일 남음</p>
    </div>
    <!-- image -->
    <div class="image-box">
    <img src="../assets/images/sampleimg1.png" alt="" />
    <!-- like -->
    <div class="like">
        <img src="../assets/icon/favorite-normal.svg" alt="" />
        <p>n.nK</p>
    </div>
    </div>

    <!-- description -->
    <div class="description">
    <p class="main truncate">
        코인플러그코인플러그코인플러그플러그플러그플러그
    </p>
    <p class="sub">코인플러그</p>

    <div class="price">
        <p class="text-sm">현재가</p>
        <p class="price-tag">1,023 <span class="text-xs">META</span></p>
        <p class="text-xs">≈₩10,000</p>
    </div>
    </div>
    </div>
`;

class PaintingItem extends HTMLElement {

  connectedCallback() {
    this.innerHTML = htmlStructure;
  }
}

customElements.define("painting-item", PaintingItem);
